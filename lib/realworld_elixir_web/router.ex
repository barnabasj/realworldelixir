defmodule RealworldElixirWeb.Router do
  use RealworldElixirWeb, :router

  pipeline :api do
    plug :accepts, ["json"]
  end

  scope "/api" do
    pipe_through :api

    forward "/graphiql", Absinthe.Plug.GraphiQL,
      schema: RealworldElixirWeb.Schema

    forward "/", Absinthe.Plug,
      schema: RealworldElixirWeb.Schema

  end
end
