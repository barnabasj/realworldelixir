defmodule RealworldElixirWeb.Resolvers.Content do

  def list_posts(_parent, _args, _resolution) do
    {:ok, RealworldElixir.Content.list_posts()}
  end
end
