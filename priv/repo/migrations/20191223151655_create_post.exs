defmodule RealworldElixir.Repo.Migrations.CreatePost do
  use Ecto.Migration

  def change do
    create table(:post) do
      add :title, :string
      add :body, :string

      timestamps()
    end

  end
end
